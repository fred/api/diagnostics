ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

Unreleased
----------

1.0.3 (2022-03-30)
------------------

* Fix package build with pyproject.toml
* Add missing changelog entries

1.0.2 (2022-03-01)
------------------

* Fix pkg-config build
* Update project setup
* Update supported python versions

1.0.1 (2021-10-13)
------------------

* Fix setup.py

1.0.0 (2020-08-08)
------------------

* Initial ``Server/Diagnostics`` API
