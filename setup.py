"""Setup script for fred_api."""
from setuptools import setup
from setuptools.command.build import build


class CustomBuild(build):
    sub_commands = [('build_grpc', None)] + build.sub_commands


setup(cmdclass={'build': CustomBuild})
